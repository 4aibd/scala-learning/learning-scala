import org.apache.spark.{SparkConf, SparkContext}
import org.apache.log4j.{Logger, Level}
import org.apache.spark.rdd.RDD

object SparkTest {
  def main(args: Array[String]): Unit = {

    Logger.getLogger("org").setLevel(Level.OFF)
    val conf: SparkConf = new SparkConf().setAppName("SparkTest").setMaster("local[2]")
    val sc = new SparkContext(conf)

    val rdd = sc.textFile("liste_villes_francaise_2015_simple.csv")

    val rdd2 = rdd.filter( element => element.split(",")(0).toInt > 1000 || element.split(",")(0).toInt < 2000 )
    val rdd3 = rdd2.filter( element => element.split(",")(1).startsWith("V") )

    rdd3.saveAsTextFile("filtered")
  }
}