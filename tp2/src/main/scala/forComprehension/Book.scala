object Book {
  def main(args: Array[String]): Unit = {
    case class Book(title: String, authors: List[String])

    def findBooksByAuthor(books: List[Book], author: String): List[Book] =
      for {book <- books
           a <- book.authors if a.startsWith(author)}
        yield book

    def findBooksContains(books: List[Book], criteria: String): List[Book] =
      for (book <- books if book.title.contains(criteria))
        yield book

    def findAuthors(books: List[Book]): List[String] =
      for {book1 <- books
           book2 <- books
           author1 <- book1.authors
           author2 <- book2.authors
           if book1 != book2
           if author1 == author2}
        yield author1

    def distinct[A](list: List[A]): List[A] =
      list.foldLeft(List[A]())((acc: List[A], e: A) => {
        if (acc.contains(e)) acc else acc :+ e
      })

    val books: List[Book] = List(
      Book("Structure and Interpretation of Computer Programs",
        List("Abelson, Harold", "Sussman, Gerald J.", "Bracha, Gilad")),
      Book("Principles of Compiler Design",
        List("Aho, Alfred", "Ullman, Jeffrey")),
      Book("Programming in Modula‑2",
        List("Wirth, Niklaus")),
      Book("Introduction to Functional Programming",
        List("Bird, Richard")),
      Book("The Java Language Specification",
        List("Gosling, James", "Joy, Bill", "Steele, Guy", "Bracha, Gilad")),
      Book("Another Book", List("Gosling, James", "Sussman"))
    )

    println(findBooksByAuthor(books, "Gosling, James"))
    println(findBooksContains(books, "Java"))
    val authors = findAuthors(books)
    println(authors)
    println(distinct[String](authors))
  }
}