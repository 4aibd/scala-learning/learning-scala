case class User(name: String, address: String){
  def addressOf(users: List[User]): List[String] = users.map(x => x.address)

  val user = List(
    User("toto", "rte"),
    User("toto", "rte"),
    User("toto", "rte")
  )
}


case class Employee(name: String, salary: Double){

  //def salarySum(employees: List[Employee]): Double = employees.reduce((x1, x2) => x1.salary + x2.salary)

  def average(values: Iterator[Double]): Double = {
    val res = values.foldLeft((0D, 0)) {
      (acc, e) => {
        val sum = acc._1
        val nb = acc._2
        (sum + e, nb + 1)
      }
    }
    res._1 / res._2
  }
  def add(o1: Option[Int], o2: Option[Int]): Option[Int] = {
    o1.flatMap(x => o2.map(y => x + y))
    for {
      v1 <- o1
      v2 <- o2
    } yield v1 + v2
  }
  

  val employees = List(
    Employee("toot", 2442D),
    Employee("toot", 2442D))
}














