package surchargeOperator

case class ComplexNumber(entier: Int, complexe: Int) {
  def getEntier: Int = entier

  def getComplexe: Int = complexe

  /** Creates a ComplexNumber with a given complexe number
    *
    *  @param that complex number
    *  @return a new ComplexNumber instance after calcul addition of 2 complex number
    */
  def +(that: ComplexNumber) = new ComplexNumber(this.entier + that.getEntier, this.complexe + that.getComplexe)


  /** Creates a ComplexNumber with a given complexe number
    *
    *  @param that complex number
    *  @return a new ComplexNumber instance after calcul soustraction of 2 complex number
    */
  def -(that: ComplexNumber) = new ComplexNumber(this.entier - that.getEntier, this.complexe - that.getComplexe)


  /** Creates a ComplexNumber with a given complexe number
    *
    *  @param that complex number
    *  @return a new ComplexNumber instance after calcul multipliate of 2 complex number
    */
  def *(that: ComplexNumber) = new ComplexNumber(
    (this.getEntier * that.getEntier) - (this.getComplexe * that.getComplexe),
    (this.getEntier * that.getComplexe) + (that.getEntier * this.getComplexe))



  override def toString: String = new String(this.getEntier + " + " + this.getComplexe + "i")
}
