package surchargeOperator

object OperatorManager extends App {

//  val operator = args(0)
//  val x1 = args(1)
//  val y1 = args(2)
//  val x2 = args(3)
//  val y2 = args(4)

  val nombre_1 = new ComplexNumber(1, 1)
  val nombre_2 = new ComplexNumber(2, 2)

  println(nombre_1)
  println(nombre_2)
  println(nombre_1 + nombre_2)

  val nombre_3 = new ComplexNumber(4, 2)
  val nombre_4 = new ComplexNumber(4, -2)

  println(nombre_3)
  println(nombre_4)
  println(nombre_3 * nombre_4)

//  val nombre_5 = new ComplexNumber(x1.toInt, y1.toInt)
//  val nombre_6 = new ComplexNumber(x2.toInt, y2.toInt)
//
//  println("calcul from args")
//  args(0) match {
//    case "+" => println(nombre_5 + nombre_6)
//    case "-" => println(nombre_5 - nombre_6)
//    case "*" => println(nombre_5 * nombre_6)
//  }
    //    //Fonctions
    //    //Trouver des fonctions ayant les signatures suivantes :
    //    val f0: () => String = ???
    //    val f1: String => String = ???
    //    val f2: (String, String) => String = ???
    //    val f3: String => String => String = ???
    //    val f4: (String => String) => String = ???
    //    val f5: String => (String => String) => String = ???

}

