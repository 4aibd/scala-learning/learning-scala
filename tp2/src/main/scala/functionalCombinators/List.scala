package functionalCombinators

import scala.sys.error

sealed trait List[+A]

case object Empty extends List[Nothing]

// Symbole de cons : functionL.Cons(val1, functionL.Cons(val2, functionL.Cons(val3, functionL.Empty)))
// Equivalent au liste chainé : val1 -> val2 -> val3
case class Cons[A](h: A, t: List[A]) extends List[A]


object List {

  def foldRight[A, B](as: List[A], b: B, f: (A, B) => B): B = as match {

    case Empty => b
    case Cons(h, t) => f(h, foldRight(t, b, f))
  }

  def foldLeft[A, B](as: List[A], b: B, f: (B, A) => B): B = as match {
    case Empty => b
    case Cons(h, t) => foldLeft(t, f(b, h), f)
  }

  // High order function
  // Val est immutable, var est mutable

  def reduceRight[A](as: List[A], f: (A, A) => A): A = as match {
    case Empty => error("bzzt. reduceRight on empty list")
    case Cons(h, t) => foldRight(t, h, f)
  }

  def reduceLeft[A](as: List[A], f: (A, A) => A): A = as match {
    case Empty => error("bzzt. reduceLeft on empty list")
    case Cons(h, t) => foldLeft(t, h, f)
  }

  def add(a: Int, b: Int): Int = a + b

  def sum(is: List[Int]): Int = {
    reduceLeft(is, (x, y) => add(x, y))
    error("todo")
  }

  def length[A](as: List[A]): Int = {
    foldLeft(as, 0, (acc: Int, e: A) => acc + 1)
  }

  // High order function
  def map[A, B](as: List[A], f: A => B): List[B] = {
    foldRight(as, Empty, (e: A, acc: List[B]) => Cons(f(e), acc))
  }

  def filter[A](as: List[A], f: A => Boolean): List[A] = {
    foldRight(as, Empty, (e: A, acc: List[A]) => if (f(e)) {
      Cons(e, acc)
    } else acc)
  }

  def append[A](x: List[A], y: List[A]): List[A] = {
    error("todo")
  }

  def flatten[A](as: List[List[A]]): List[A] = {
    reduceLeft(as, (l1: List[A], l2: List[A]) => concat(l1, l2))
  }

  def concat[A](x: List[A], y: List[A]): List[A] = {
    foldRight(x, y, (e: A, acc: List[A]) => Cons(e, acc))
  }

  def flatMap[A, B](as: List[A], f: A => List[B]): List[B] = {
    flatten(map(as, f))
  } // functionL.List[B]

  // is functionL.List de Int >= 0
  def maximum(is: List[Int]): Int = foldLeft(is, 0, (acc: Int, e: Int) => if (e > acc) e else acc)


  def reverse[A](as: List[A]): List[A] = foldLeft(as, Empty, (acc: List[A], e: A) => Cons(e, acc))

}

