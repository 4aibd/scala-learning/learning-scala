class MessageClass(var id: Int, var typ: String, var content: String){ // Utiliser var pour rendre mutable
  override def toString: String = s"Le message n°$id de type ${typ.toUpperCase()} contient : $content"

  override def equals(obj: Any): Boolean = (typ == obj.asInstanceOf[MessageClass].typ) && (content == obj.asInstanceOf[MessageClass].content)
}

val msg1 = new MessageClass(1, "sms", "cc toi")
val msg2 = new MessageClass(1, "email", "blabla")
val msg3 = new MessageClass(1, "sms", "cc toi")

println(msg1.toString)

msg2.equals(msg1)
msg1.equals(msg3)

println(msg2.content)
msg2.content = "bloublou"
println(msg2.content)

case class MessageCaseClass(id: Int, typ: String, content: String)

val msg4 = MessageCaseClass(1, "sms", "cc toi")
val msg5 = MessageCaseClass(2, "email", "blabla")
val msg6 = MessageCaseClass(1, "sms", "cc toi")

msg4.equals(msg5)
msg4.equals(msg6)