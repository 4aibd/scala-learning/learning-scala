class MessageClass(var id: Int = 0,var name: String,var content: String)
{
    override def toString: String=
    s"(le message n $id de type $name contient : $content)"
}

// b. Instancier 3 nouveaux messages

// i. msg1 avec id = 1, type = “sms” et content = “cc toi”
val msg1 = new MessageClass(1,"sms","cc toi")

// ii. msg2 avec id = 2, type = “email” et content = “C’est un email, cordialement”
val msg2 = new MessageClass(2,"email","C’est un email, cordialement")

// iii. msg3 avec id = 1, type = “sms” et content = “cc toi”
val msg3 = new MessageClass(1,"sms","cc toi")

// c. Tester la surcharge de votre méthode ​toString​ :
println(msg1.toString())

// d. Exécuter les lignes suivantes
msg2.equals(msg1)
msg1.equals(msg3)

// Exécuter l’expression suivante:
println(msg2.content)

// Comment appelle-t-on ce genre de méthode en Java (ou dans d’autre langage OO)? 
surcharge

// Modifier le ​content​ du ​msg2​ par “Voici mon mot de passe: P@ssw0rd”. Vérifier que le changement ait bien été effectué.
val msg2 = new MessageClass(2,"email","Voici mon mot de passe: P@ssw0rd")
println(msg2.content)

// f. Surcharger désormais la méthode ​equals​ afin de vérifier égalité des valeurs sauf de l’id dans votre définition de classe et recréer les 3 messages
class MessageClass(var id: Int = 0,var name: String,var content: String)
{
    override def toString: String=
    s"(le message n $id de type $name contient : $content)"

    def canEqual(a: Any) = a.isInstanceOf[MessageClass]
    override def equals(that: Any): Boolean =
        that match {
            case that: MessageClass => that.name.equals(this.name) && that.content.equals(this.content)
            case _ => false
     }

}

val msg1 = new MessageClass(1,"sms","cc toi")
val msg2 = new MessageClass(2,"email","C’est un email, cordialement")
val msg3 = new MessageClass(1,"sms","cc toi")

// Vérif
msg2.equals(msg1)
msg1.equals(msg3)


// g. Refaire ​d.​, nous avons désormais une méthode ​equals​ qui compare par valeur et non par référence.

//  2. Définir une nouvelle Case Class nommée “Message” qui a 3 paramètres :

//  a. id de type ​Int

// b. type de type ​String

// c. content de type ​String