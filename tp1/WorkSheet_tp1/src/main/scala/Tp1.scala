class MessageClass(var id: Int = 0,var name: String,var content: String)
{
  override def toString: String=
    s"(le message n $id de type $name contient : $content)"

  def canEqual(a: Any) = a.isInstanceOf[MessageClass]
  override def equals(that: Any): Boolean =
    that match {
      case that: MessageClass => that.name.equals(this.name) && that.content.equals(this.content)
      case _ => false
    }
}

object tp1 {

  def main(args: Array[String]): Unit = {
    val msg1 = new MessageClass(1,"sms","cc toi")
    val msg2 = new MessageClass(2,"email","C’est un email, cordialement")
    val msg3 = new MessageClass(1,"sms","cc toi")
    println(msg1)
    println(msg2)
    println(msg3)

    // Vérif
    msg2.equals(msg1)
    msg1.equals(msg3)
  }
}
